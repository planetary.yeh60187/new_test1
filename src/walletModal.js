import { useState } from 'react'
import WalletConnect from "@walletconnect/client";
import QRCodeModal from "@walletconnect/qrcode-modal";

let connector;
let accounts, chainId;
let stat = false;
const walletConnectInit_func = walletConnectInit;
const walletbuyToken_func = walletbuyToken;
function puttab2(newarr){
    //找到table标签
    var tab_1 = document.getElementById('walletConnectInit_btn');

    tab_1.innerText=newarr;

}
function puttab3(newarr){
    //找到table标签
    var tab_1 = document.getElementById('tab3');

    tab_1.innerText=newarr;

}
async function walletbuyToken()
{
    if(!stat) {
        puttab3("钱包未连接，无法交易");
        return null;
    }
    const tx = {
        from: accounts.toString(), // Required
        to: "0x0a5f205f495ee99c4fc73aeacd73763d114b807a", // Required (for non contract deployments)
        data: "0xa9059cbb000000000000000000000000b47643eb3ad3e074ffc7625bf8791458b15e6b3900000000000000000000000000000000000000000000000000005afabf6e4a00", // Required
        value: "0x00", // Optional
    };

// Send transaction
    connector
        .sendTransaction(tx)
        .then((result) => {
            // Returns transaction id (hash)
            puttab3(result);

        })
        .catch((error) => {
            // Error returned when rejected
            puttab3(error);
        });
}

async function walletConnectInit() {
    // Create a connector

    connector = new WalletConnect({
        bridge: "https://bridge.walletconnect.org", // Required
        qrcodeModal: QRCodeModal,
    });

// Check if connection is already established
    if (!connector.connected) {
        // create new session
        connector.createSession();
    }

// Subscribe to connection events
    connector.on("connect", (error, payload) => {
        if (error) {
            throw error;
        }
        stat = true;
        // Get provided accounts and chainId

        ({accounts, chainId} = payload.params[0]);
        puttab2("connect=>" + accounts)
        //window.alert("connect" + accounts);
    });

    connector.on("session_update", (error, payload) => {
        if (error) {
            throw error;
        }
        // Get updated accounts and chainId
        ({accounts, chainId} = payload.params[0]);
        puttab2("update=>" + accounts)
    });

    connector.on("disconnect", (error, payload) => {
        if (error) {
            throw error;
        }
        stat = false;
        //const { accounts, chainId } = payload.params[0];
        puttab2("disconnect")
        // Delete connector
    });
}

function walletModal(modl) {
    if(modl=="init")
    {

    }
if(modl=="walletConnectInit")
   return walletConnectInit_func;
    if(modl=="walletbuyToken")
    return walletbuyToken_func;
    else
        return connector;
}

export default walletModal;

